#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include<unistd.h>

#define BUFSIZE 1024

int main(int argc, char* argv[]) {
    int inFd, outFd;
    char buf[BUFSIZE];

    inFd = open(argv[1], O_RDONLY);
    outFd = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);

    ssize_t numRead;

    while ((numRead = read(inFd, buf, BUFSIZE)) > 0) {
        write(outFd, buf, numRead);
    }


    close(inFd);
    close(outFd);

    return 0;
}
