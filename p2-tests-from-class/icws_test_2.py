import socket as sk
from time import sleep

## legit request

##receive each byte
def legit0 ():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))

        ## at this point, it's connected!
        s.sendall(b"G")
        sleep(1)
        s.sendall(b"E")
        sleep(1)
        s.sendall(b"T")
        sleep(1)
        s.sendall(b" ")
        sleep(1)
        s.sendall(b"/")
        sleep(1)
        s.sendall(b" ")
        sleep(1)
        s.sendall(b"H")
        sleep(1)
        s.sendall(b"T")
        sleep(1)
        s.sendall(b"T")
        sleep(1)
        s.sendall(b"P")
        sleep(1)
        s.sendall(b"/")
        sleep(1)
        s.sendall(b"1")
        sleep(1)
        s.sendall(b".")
        sleep(1)
        s.sendall(b"1")
        sleep(1)
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(1)
        s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
        sleep(0.5)
        s.sendall(b"Connection: close\r\n\r\n")

        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)

##get /r then  /n
def legit1() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))

        ## at this point, it's connected!
        s.sendall(b"GET / HTTP/1.1")
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(1)
        s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
        sleep(0.5)
        s.sendall(b"Connection: close\r\n\r\n")

        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)
##request different host name
def legit2() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))

        ## at this point, it's connected!
        s.sendall(b"GET / HTTP/1.1")
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(5)
        s.sendall(b"Host: Micro\r\n")
        sleep(0.5)
        s.sendall(b"Connection: close\r\n\r\n")

        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)
            
##HEAD Request check 
def legit3() :
     with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))

        ## at this point, it's connected!
        s.sendall(b"H")
        sleep(1)
        s.sendall(b"E")
        sleep(1)
        s.sendall(b"A")
        sleep(1)
        s.sendall(b"D")
        sleep(1)
        s.sendall(b" ")
        sleep(1)
        s.sendall(b"/")
        sleep(1)
        s.sendall(b"in")
        sleep(1)
        s.sendall(b"de")
        sleep(1)
        s.sendall(b"x")
        sleep(1)
        s.sendall(b".")
        sleep(1)
        s.sendall(b"ht")
        sleep(1)
        s.sendall(b"ml")
        sleep(1)
        s.sendall(b" ")
        sleep(1)
        s.sendall(b"H")
        sleep(1)
          s.sendall(b"T")
        sleep(1)
          s.sendall(b"T")
        sleep(1)
          s.sendall(b"P")
        sleep(1)
          s.sendall(b"/")
        sleep(1)
          s.sendall(b"1")
        sleep(1)
          s.sendall(b".")
        sleep(1)
           s.sendall(b"1")
        sleep(1)
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(1)
        s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
        sleep(0.5)
        s.sendall(b"Accept: text/html\r\n\r\n")

        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)
            
##Head with /r/n
def legit4() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))

        ## at this point, it's connected!
        s.sendall(b"HEAD /index.html HTTP/1.1")
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(5)
          s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
        sleep(0.5)
             s.sendall(b"Accept: text/html\r\n\r\n")

        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)
            
##Head with different host name 
def legit5() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))

        ## at this point, it's connected!
       s.sendall(b"HEAD /index.html HTTP/1.1")
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(5)
        s.sendall(b"Host: Micro\r\n")
        sleep(0.5)
        s.sendall(b"Connection: close\r\n\r\n")

        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)
            
##wrong connection/ /domain name/ server-port
def legit6() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 800000000))

        ## at this point, it's connected!
        s.sendall(b"GET / HTTP/1.1")
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(5)
       s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
        sleep(0.5)
        s.sendall(b"Connection: keep-alive\r\n\r\n")

        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)
            
##illegal
## creative
##Invalid method
def creative0() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))

        ## at this point, it's connected!
        s.sendall(b"GEE")
        sleep(0.5)
        s.sendall(b" / ") 
        sleep(0.5)
        s.sendall(b"HTTP") 
        sleep(0.5)
        s.sendall(b"/1.1") 
        sleep(0.5)
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(5)
        s.sendall(b"Host: Micro\r\n")
        sleep(0.5)
        s.sendall(b"Connection: close\r\n\r\n")

        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)
##invalid ending
def creative1() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))        
        s.sendall(b"HEAD / HTTP/1.1\r\n")
        ## at this point, it's connected!

        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)

##only \r\n
def creative2() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))
        ## at this point, it's connected!
        s.sendall(b"\r\n\r\n");
        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)

##weird connection
def creative3() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))
        ## at this point, it's connected!
        s.sendall(b"GET / HTTP/1.1")
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(1)
        s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
        sleep(0.5)
        s.sendall(b"Connection: foo\r\n\r\n")
        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)


##different HTTP version 
def creative4() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))
        ## at this point, it's connected!
        s.sendall(b"GET / HTTP/0.0")
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(1)
        s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
        sleep(0.5)
        s.sendall(b"Connection: close\r\n\r\n")
        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)


##unknown requests/method
def creative5() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))
        ## at this point, it's connected!
        s.sendall(b" / HTTP/1.1")
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(1)
        s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
        sleep(0.5)
        s.sendall(b"Connection: close\r\n\r\n")
        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)


##white spaces check
def creative6() :
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))
        ## at this point, it's connected!
        s.sendall(b"    GET /     HTTP/1.1   /n")
        s.sendall(b"\r")
        sleep(1)
        s.sendall(b"\n")
        sleep(1)
        s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
        sleep(0.5)
        s.sendall(b"Connection: close\r\n\r\n")
        while data := s.recv(1024):
            stringdata = data.decode('utf-8')
            print(stringdata)

def main():
    ##for testing
if __name__ == "__main__":
    main()
