# Name : Apiwit Chonkitgosol 6280561
# Name : Yossatorn Phithakjiraroj u6280942

import socket as sk
from time import sleep

# Legal Request

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost', 80))

    ## at this point, it's connected!
    s.sendall(b"GET")
    sleep(1)
    s.sendall(b" / HT")
    sleep(0.5)
    s.sendall(b"TP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\n")
    s.sendall(b"Host: localhost\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

print('\n')

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('cs.muic.mahidol.ac.th', 80))

    ## at this point, it's connected!
    s.sendall(b"GET")
    sleep(1)
    s.sendall(b" / HT")
    sleep(0.5)
    s.sendall(b"TP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\n")
    s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

print('\n')

import http.client

conn = http.client.HTTPConnection(host='localhost', port=80)

conn.set_debuglevel(1)

headers = {
    'Accept': 'text/plain',
    'Blah': 'blahblah',
    'Connection': 'close',
}

conn.request('GET', '/', headers=headers)
resp = conn.getresponse()

print('\n')

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('cs.muic.mahidol.ac.th', 80))

    ## at this point, it's connected!
    s.sendall(b"HEAD")
    sleep(1)
    s.sendall(b" / HT")
    sleep(0.5)
    s.sendall(b"TP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\n")
    s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

print('\n')

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('cs.muic.mahidol.ac.th', 80))

    ## at this point, it's connected!
    s.sendall(b"GET")
    sleep(1)
    s.sendall(b" / HT")
    sleep(0.5)
    s.sendall(b"TP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\n")
    sleep(0.5)
    s.sendall(b"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n")
    s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

print('\n')

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('muic.mahidol.ac.th', 80))

    ## at this point, it's connected!
    s.sendall(b"POST")
    sleep(1)
    s.sendall(b" / HT")
    sleep(0.5)
    s.sendall(b"TP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\n")
    sleep(0.5)
    s.sendall(b"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n")
    s.sendall(b"Host: muic.mahidol.ac.th\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

print('\n')


with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('muic.mahidol.ac.th', 80))

    ## at this point, it's connected!
    s.sendall(b"HEAD")
    sleep(1)
    s.sendall(b" / HT")
    sleep(0.5)
    s.sendall(b"TP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\n")
    sleep(0.5)
    s.sendall(b"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n")
    s.sendall(b"Host: muic.mahidol.ac.th\r\n")
    sleep(0.5)
    s.sendall(b"Connection: Keep-Alive\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)


# Illegal Request
print('\n')
print('Illegal Request -v-')

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('muic.mahidol.ac.th', 80))

    ## at this point, it's connected!
    s.sendall(b"I love Ajarn Kanat")
    sleep(1)
    s.sendall(b" / HT")
    sleep(0.5)
    s.sendall(b"TP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\n")
    sleep(0.5)
    s.sendall(b"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n")
    s.sendall(b"Host: muic.mahidol.ac.th\r\n")
    sleep(0.5)
    s.sendall(b"Connection: Keep-Alive\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

print('\n')

# 404 Not Found
mysock = sk.socket(sk.AF_INET,sk.SOCK_STREAM)
mysock.connect(('www.py4inf.com',80))
mysock.send(b'GET http://www.py4inf.com/code/romeo.txt HTTP/1.0\n\n')
while True:
    data = mysock.recv(512)
    if (len(data)) < 1:
        break
    print(data)
mysock.close()

print('\n')

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('muic.mahidol.ac.th', 80))

    ## at this point, it's connected!
    s.sendall(b"GET")
    sleep(1)
    s.sendall(b" / HT")
    sleep(0.5)
    s.sendall(b"TP/1.1")
    sleep(0.5)
    s.sendall(b"\n")
    sleep(0.5)
    s.sendall(b"\n")
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"User-Agent: Mozilla/ (compatible; MSIE5.01; Windows NT)\r\n")
    s.sendall(b"Host: muic.mahidol.ac.th\r\n")
    sleep(0.5)
    s.sendall(b"Connection: Keep-Alive\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

print("\n")

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('muic.mahidol.ac.th', 80))

    ## at this point, it's connected!
    s.sendall(b"GET/HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\n")
    sleep(0.5)
    s.sendall(b"User-Agent: Mozilla/ (compatible; MSIE5.01; Windows NT)\r\n")
    s.sendall(b"Host: muic.mahidol.ac.th\r\n")
    sleep(0.5)
    s.sendall(b"Connection: Keep-Alive\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

print("\n")

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('muic.mahidol.ac.th', 80))
    s.shutdown

    ## at this point, it's connected!
    s.sendall(b"GET/HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\n")
    sleep(0.5)
    s.sendall(b"User-Agent: Mozilla/ (compatible; MSIE5.01; Windows NT)\r\n")
    s.sendall(b"Host: muic.mahidol.ac.th\r\n")
    sleep(0.5)
    s.sendall(b"Connection: Keep-Alive\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

print("\n")

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('muic.mahidol.ac.th', 80))

    ## at this point, it's connected!
    s.sendall(b"GET/HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    s.sendall(b"\n")
    sleep(0.5)
    s.gettimeout()
    s.sendall(b"User-Agent: Mozilla/ (compatible; MSIE5.01; Windows NT)\r\n")
    s.sendall(b"Host: muic.mahidol.ac.th\r\n")
    sleep(0.5)
    s.sendall(b"Connection: Keep-Alive\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

print("\n")

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('muic.mahidol.ac.th', 80))

    ## at this point, it's connected!
    s.sendall(b"GET/HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    s.sendall(b"\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)