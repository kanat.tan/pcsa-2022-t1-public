#! /usr/bin/python

import http.client
import socket as sk
from time import sleep

#Illegal
web = 'cs.muic.io'

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect((web, 80))
    print("\n---ILL 1---\n")
    ## at this point, it's connected!
    s.sendall(b"GE")
    s.sendall(b"T/ HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n") % (web)
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)
        
        
    print("\n---ILL 2---\n")
    s.sendall(b"GET ")
    s.sendall(b"/ HTTP/")
    sleep(0.7)
    s.sendall(b"1.1\r")
    sleep(0.5)
    s.sendall(b"\n")
    s.sendall(b"Host: %s\r\n") % (web)
    sleep(0.5)
    s.sendall(b"Connection: clo")
    sleep(0.5)
    s.sendall(b"se\r\n")
    sleep(1)
    s.sendall(b"\r\n")
    
    while data := s.recv(1024):
        print(data)
        
    print("\n---ILL 3---\n")
    s.sendall(b"GET")
    sleep(0.6)
    s.sendall(b" ")
    s.sendall(b"/ HTTP/1.1\r\n")
    sleep(2)
    s.sendall(b"Host: %s\r\n") % (web)
    sleep(0.5)
    s.sendall(b"Connection: ")
    sleep(3)
    s.sendall(b"close\r\n\r")
    sleep(1)
    s.sendall(b"\r\n")
    
    while data := s.recv(1024):
        print(data)
        
    print("\n---ILL 4---\n")
    s.sendall(b"GET / HTTP/1.1\r\n")
    sleep(6)
    s.sendall(b"Host: %s\r\n") % (web)
    sleep(7)
    s.sendall(b"Connection: close\r\n\r")
    sleep(3)
    sleep(4)
    s.sendall(b"\n")
    
    while data := s.recv(1024):
        print(data)
        
    print("\n---ILL 5---\n")
    s.sendall(b"G")
    sleep(0.2)
    s.sendall(b"E")
    sleep(0.5)
    s.sendall(b"T")
    sleep(0.4548)
    s.sendall(b" ")
    sleep(1.489)
    s.sendall(b"/ HTTP/")
    sleep(0.7)
    s.sendall(b"1.")
    sleep(0.5)
    s.sendall(b"1")
    sleep(0.5)
    s.sendall(b"\r\n")
    sleep(0.5)
    s.sendall(b"Host:")
    sleep(0.5)
    s.sendall(b" %s\r") % (web)
    sleep(0.5)
    s.sendall(b"\n")
    sleep(0.5)
    s.sendall(b"Connection: clo")
    sleep(0.5)
    s.sendall(b"se\r\n")
    sleep(1)
    s.sendall(b"\r\n")
    
    while data := s.recv(1024):
        print(data)
        
    print("\n---ILL 6---\n")
    s.sendall(b"HEAD / HTTP/1.1\r\r")
    sleep(6)
    s.sendall(b"\nHost: %s\r\n") % (web)
    sleep(0.5)
    sleep(4)
    s.sendall(b"Connection: close\r\n\r")
    sleep(3)
    
    s.sendall(b"\n")
    
    while data := s.recv(1024):
        print(data)
        
    print("\n---ILL 7---\n")
    s.sendall(b"HEAD / HTTP/1.1\r\r\nHost: %s\r\n") % (web)
    sleep(10)
    s.sendall(b"Connection: close\r\n")
    sleep(3)
    s.sendall(b"\r\n")
    
    while data := s.recv(1024):
        print(data)
        
    #legal
    
    conn = http.client.HTTPConnection(host='www.google.com', port=80)

conn.set_debuglevel(1)

print("\n----Leg 1----\n")
headers = {
    'Accept': 'text/html',
    'Blah': 'blahblah',
    'Connection': 'close',
}

conn.request('HEAD', '/', headers=headers)
resp = conn.getresponse()


print('-----------------------')
print(resp.status, resp.reason)
print(resp.headers)
print(resp.read())

print("\n----Leg 2----\n")
headers = {
    'Accept': 'text/html',
    'Host': f'{web}',
    'Connection': 'close',
}

conn.request('HEAD', '/', headers=headers)
resp = conn.getresponse()


print('-----------------------')
print(resp.status, resp.reason)
print(resp.headers)
print(resp.read())

print("\n----Leg 4----\n")
s.sendall(b"GET / HTTP/1.1\r\n")
s.sendall(b"Host: %s\r\n") % (web)
s.sendall(b"Connection: close\r\n\r\n")

while data := s.recv(1024):
    print(data)
    
s.sendall(b"HEAD / HTTP/1.1\r\n")
s.sendall(b"Host: %s\r\n") % (web)
s.sendall(b"Connection: close\r\n\r\n")

print("\n----Leg 5----\n")
while data := s.recv(1024):
    print(data)
    
headers = {
    'Accept': 'text/html',
    'Blah': 'blahblah',
    'Connection': 'keep-alive',
}

conn.request('HEAD', '/', headers=headers)
resp = conn.getresponse()


print('-----------------------')
print(resp.status, resp.reason)
print(resp.headers)
print(resp.read())

print("\n----Leg 6----\n")
headers = {
    'Accept': 'text/html',
    'Host': f'{web}',
    'Accept-Language': 'en-us,en;q=0.5\r\n',
    'Connection': 'close',
}

conn.request('HEAD', '/', headers=headers)
resp = conn.getresponse()


print('-----------------------')
print(resp.status, resp.reason)
print(resp.headers)
print(resp.read())

print("\n----Leg 7----\n")
s.sendall(b"HEAD / HTTP/1.1\r\n")
s.sendall(b"Host: %s\r\n") % (web)
s.sendall(b"Accept: text/html,application/xhtml+xml\r\n")
s.sendall(b"Connection: close\r\n\r\n")

while data := s.recv(1024):
    print(data)