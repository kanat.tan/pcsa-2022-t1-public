import socket as sk
from time import sleep
import sys

#ARGV[1] = host_name
#ARGV[2] = port

def main():
    test_functions = [validtest1, validtest2, validtest3, validtest4, validtest5, validtest6, validtest7]
    for i in range(len(test_functions)):
        with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
            s.connect((sys.argv[1], int(sys.argv[2])))

            test_functions[i](s)

            while data := s.recv(1024):
                print(data)
                print("")

def main2():
    test_functions = [illegaltest1, illegaltest2, illegaltest3, illegaltest4, illegaltest5, illegaltest6, illegaltest7]
    for i in range(len(test_functions)):
        with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
            s.connect((sys.argv[1], int(sys.argv[2])))

            test_functions[i](s)

            while data := s.recv(1024):
                print(data)
                print("")


def validtest1(s):
    ## at this point, it's connected!
    s.sendall(b"GE")
    sleep(1)
    s.sendall(b"T / HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")


def validtest2(s):
    s.sendall(b"GET")
    sleep(1)
    s.sendall(b" / HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")


def validtest3(s):
    s.sendall(b"GET")
    sleep(1)
    s.sendall(b" /cat.jpg HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")


def validtest4(s):
    s.sendall(b"GET")
    sleep(2)
    s.sendall(b" / HTTP/1.1")
    sleep(4)
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    sleep(8)
    s.sendall(b"Connection: close\r\n\r\n")


def validtest5(s):
    s.sendall(b"HEAD")
    sleep(1)
    s.sendall(b" / HTTP/1.1\r\n")
    sleep(0.5)
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    s.sendall(b"\r\n")


def validtest6(s):
    s.sendall(b"GET")
    s.sendall(b" / HTTP/1.1")
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    s.sendall(b"Connection: close\r\n\r\n")


def validtest7(s):
    s.sendall(b"GET")
    sleep(1)
    s.sendall(b" /index.html HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")


def illegaltest1(s):
    s.sendall(b"GE")
    sleep(1)
    s.sendall(b"T / HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

def illegaltest2(s):
    s.sendall(b"GE")
    sleep(1)
    s.sendall(b"T / HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

def illegaltest3(s):
    s.sendall(b"GE")
    sleep(1)
    s.sendall(b"T / HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

def illegaltest4(s):
    s.sendall(b"GE")
    sleep(1)
    s.sendall(b"TTT / HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

def illegaltest5(s):
    s.sendall(b"GET")
    s.sendall(b" / HTTP/1.12")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")

def illegaltest6(s):
    s.sendall(b"GET / HTTP/1.1")
    sleep(1)
    s.sendall(b"\n")
    sleep(1)
    s.sendall(b"Host: %s\n" % bytes(sys.argv[1], 'ascii'))
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")


def illegaltest7(s):
    s.sendall(b"GET")
    s.sendall(b"./ HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r\n")
    s.sendall(b"Host: %s\r\n" % bytes(sys.argv[1], 'ascii'))
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")



main()
main2()