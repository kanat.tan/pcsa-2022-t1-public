import http.client
import socket as sk
from time import sleep

conn = http.client.HTTPConnection(host='cs.muic.mahidol.ac.th', port=80)
conn.set_debuglevel(1)
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('cs.muic.mahidol.ac.th', 80))
    ## at this point, it's connected!
    s.sendall(b"GE")
    sleep(1)
    s.sendall(b"T / HTTP/1.1")
    sleep(0.5)
    s.sendall(b"\r")
    sleep(0.5)
    s.sendall(b"\r\n")
    s.sendall(b"Host: cs.muic.mahidol.ac.th\r\n")
    sleep(0.5)
    s.sendall(b"Connection: close\r\n\r\n")
    while data := s.recv(1024):
        print(data)

type = 0
match type:
    case 1:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': 'text/html',
        }
    case 2:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': 'text/css',
        }
    case 3:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': 'text/plain',
        }
    case 4:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': 'text/javascript',
        }
    case 5:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': 'image/jpg',
        }
    case 6:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': 'image/png',
        }
    case 7:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': 'image/gif',
        }
    case 8:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': '',
        }
    case 9:
        headers = {
            'Server': '',
            'Connection': 'close',
            'Content-type': '',
        }
    case 10:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': 'image/gif',
        }
    case 11:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': 'blahblah',
        }
    case 12:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': 'image/csss',
        }
    case 13:
        headers = {
            'Server': 'icws',
            'Connection': 'open',
            'Content-type': '',
        }
    case 14:
        headers = {
            'Server': 'icws',
            'Connection': 'close',
            'Content-type': 'image',
        }
count = 0
while count <= 14:
    type+=1
    conn.request('GET', '/', headers=headers)
    resp = conn.getresponse()
    print('-----------------------')
    print(resp.status, resp.reason)
    print(resp.headers)
    print(resp.read())

    count+= 1








